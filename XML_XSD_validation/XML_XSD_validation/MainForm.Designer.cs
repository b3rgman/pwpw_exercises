﻿namespace XML_XSD_validation
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ButtonsGroupBox = new System.Windows.Forms.GroupBox();
            this.ValidateButton = new System.Windows.Forms.Button();
            this.ChooseXSDFileButton = new System.Windows.Forms.Button();
            this.XSDFileLabel = new System.Windows.Forms.Label();
            this.ChooseXMLFileButton = new System.Windows.Forms.Button();
            this.XMLfileLabel = new System.Windows.Forms.Label();
            this.ValidationLabel = new System.Windows.Forms.Label();
            this.CheckErrorsButton = new System.Windows.Forms.Button();
            this.ButtonsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonsGroupBox
            // 
            resources.ApplyResources(this.ButtonsGroupBox, "ButtonsGroupBox");
            this.ButtonsGroupBox.Controls.Add(this.ValidateButton);
            this.ButtonsGroupBox.Controls.Add(this.ChooseXSDFileButton);
            this.ButtonsGroupBox.Controls.Add(this.XSDFileLabel);
            this.ButtonsGroupBox.Controls.Add(this.ChooseXMLFileButton);
            this.ButtonsGroupBox.Controls.Add(this.XMLfileLabel);
            this.ButtonsGroupBox.Name = "ButtonsGroupBox";
            this.ButtonsGroupBox.TabStop = false;
            this.ButtonsGroupBox.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // ValidateButton
            // 
            resources.ApplyResources(this.ValidateButton, "ValidateButton");
            this.ValidateButton.Name = "ValidateButton";
            this.ValidateButton.UseVisualStyleBackColor = true;
            this.ValidateButton.Click += new System.EventHandler(this.ValidateButton_Click);
            // 
            // ChooseXSDFileButton
            // 
            resources.ApplyResources(this.ChooseXSDFileButton, "ChooseXSDFileButton");
            this.ChooseXSDFileButton.Name = "ChooseXSDFileButton";
            this.ChooseXSDFileButton.UseVisualStyleBackColor = true;
            this.ChooseXSDFileButton.Click += new System.EventHandler(this.Button1_Click);
            // 
            // XSDFileLabel
            // 
            resources.ApplyResources(this.XSDFileLabel, "XSDFileLabel");
            this.XSDFileLabel.Name = "XSDFileLabel";
            // 
            // ChooseXMLFileButton
            // 
            resources.ApplyResources(this.ChooseXMLFileButton, "ChooseXMLFileButton");
            this.ChooseXMLFileButton.Name = "ChooseXMLFileButton";
            this.ChooseXMLFileButton.UseVisualStyleBackColor = true;
            this.ChooseXMLFileButton.Click += new System.EventHandler(this.ChooseXMLbutton_Click);
            // 
            // XMLfileLabel
            // 
            resources.ApplyResources(this.XMLfileLabel, "XMLfileLabel");
            this.XMLfileLabel.Name = "XMLfileLabel";
            this.XMLfileLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // ValidationLabel
            // 
            resources.ApplyResources(this.ValidationLabel, "ValidationLabel");
            this.ValidationLabel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ValidationLabel.Name = "ValidationLabel";
            // 
            // CheckErrorsButton
            // 
            resources.ApplyResources(this.CheckErrorsButton, "CheckErrorsButton");
            this.CheckErrorsButton.Name = "CheckErrorsButton";
            this.CheckErrorsButton.UseVisualStyleBackColor = true;
            this.CheckErrorsButton.Click += new System.EventHandler(this.CheckErrorsButton_Click);
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CheckErrorsButton);
            this.Controls.Add(this.ValidationLabel);
            this.Controls.Add(this.ButtonsGroupBox);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.ButtonsGroupBox.ResumeLayout(false);
            this.ButtonsGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox ButtonsGroupBox;
        private System.Windows.Forms.Label XMLfileLabel;
        private System.Windows.Forms.Button ChooseXMLFileButton;
        private System.Windows.Forms.Button ChooseXSDFileButton;
        private System.Windows.Forms.Label XSDFileLabel;
        private System.Windows.Forms.Button ValidateButton;
        private System.Windows.Forms.Label ValidationLabel;
        private System.Windows.Forms.Button CheckErrorsButton;
    }
}

