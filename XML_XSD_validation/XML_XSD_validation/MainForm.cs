﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Schema;

namespace XML_XSD_validation
{
    public partial class MainForm : Form
    {   
        private String ErrorMSG = "";
        private String XMLpath = "";
        private String XSDpath = "";
        public MainForm()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ChooseXMLbutton_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            
            file.Filter = "XML files|*.xml";
            if (file.ShowDialog() == DialogResult.OK)
            {
                var path = file.FileName;
                var name = file.SafeFileName;
                XMLpath = path;
                ChooseXMLFileButton.Text = name.Remove(name.Length - 4);
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "XSD files|*.xsd";
            if (file.ShowDialog() == DialogResult.OK)
            {
                var path = file.FileName;
                var name = file.SafeFileName;
                XSDpath = path;
                ChooseXSDFileButton.Text = name.Remove(name.Length - 4);
            }
        }

        private void ValidateButton_Click(object sender, EventArgs e)
        {
            ErrorMSG = "";
            if (XSDpath.Length == 0)
                Button1_Click(null, null);
            if (XMLpath.Length == 0)
                ChooseXMLbutton_Click(null, null);
            if (XMLpath.Length > 0 && XSDpath.Length > 0)
            {
                Validate(XMLpath, XSDpath);

                if (ErrorMSG.Length > 0)
                {
                    ValidationLabel.Text = "Incorrect";
                    CheckErrorsButton.Visible = true;

                }
                else
                {
                    ValidationLabel.Text = "Correct";
                    CheckErrorsButton.Visible = false;

                }

            }
        }


        public void Validate(string xmlPath, string xsdPath)
        {

            XmlReaderSettings XSDSettings = new XmlReaderSettings();
            XSDSettings.Schemas.Add(null, xsdPath);
            XSDSettings.ValidationType = ValidationType.Schema;
            XSDSettings.ValidationEventHandler += new ValidationEventHandler((e,z)=> {
                if (z.Severity == XmlSeverityType.Error)
                {
                    ErrorMSG += (z.Message);
                }
            });

            XmlReader xml = XmlReader.Create(xmlPath, XSDSettings);
            while (xml.Read()) ;


        }

        private void CheckErrorsButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(ErrorMSG,"Errors");

        }
    }
}
